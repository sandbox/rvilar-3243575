<?php

namespace Drupal\acrelianews;

use Drupal\Core\Site\Settings;
use Ymbra\Acrelianews\AcrelianewsContact;

/**
 * Class ContactService.
 *
 * @package Drupal\acrelianews
 */
class ContactService {

  /**
   * Default email format.
   */
  const DEFAULT_EMAIL_FORMAT = 2;

  /**
   * Positive value.
   */
  const POSITIVE_VALUE = 'Yes';

  /**
   * Negative value.
   */
  const NEGATIVE_VALUE = 'No';

  /**
   * Drupal\Core\Site\Settings definition.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Ymbra\Acrelianews\AcrelianewsContact definition.
   *
   * @var \Ymbra\Acrelianews\AcrelianewsContact
   */
  protected $client;

  /**
   * Constructs a new ContactService object.
   */
  public function __construct(Settings $settings) {
    $this->settings = $settings;
    $this->client = new AcrelianewsContact($this->settings->get('acrelianews_api_key'));
  }

  /**
   * Creates new contact.
   *
   * @param array $data
   *   Contact data.
   */
  public function create(array $data) {
    try {
      $this->client->add(
        $this->settings->get('acrelianews_contacts_list_id'),
        $data['email_address'],
        static::DEFAULT_EMAIL_FORMAT,
        $data['custom_fields']
      );
    }
    catch (\Exception $e) {
      watchdog_exception('acrelianews', $e);
    }
  }

  /**
   * Gets contact by email.
   *
   * @param string $email
   *   Contact email.
   *
   * @return array|null
   *   Contact data.
   */
  public function get(string $email) {
    $response = json_decode($this->client->getByEmail(
      $this->settings->get('acrelianews_contacts_list_id'),
      $email
    ));

    if (property_exists($response, 'errors')) {
      return NULL;
    }

    return $response;
  }

  /**
   * Gets negative value in Acrealianews.
   *
   * @return string
   *   The String of negative value.
   */
  public function getNegativeValue() {
    return static::NEGATIVE_VALUE;
  }

  /**
   * Gets positive value in Acrealianews.
   *
   * @return string
   *   The String of positive value.
   */
  public function getPositiveValue() {
    return static::POSITIVE_VALUE;
  }

  /**
   * Gets qualifications keys.
   *
   * @return string
   *   Array of qualification keys.
   */
  public function getQualificationsKeys() {
    return [
      'role',
      'role2',
      'role3',
      'role-4',
      'role-5',
    ];
  }

  /**
   * Updates contact.
   *
   * @param array $data
   *   Contact data.
   */
  public function update(array $data) {
    try {
      $this->client->edit(
        $this->settings->get('acrelianews_contacts_list_id'),
        $data['email_address'],
        1,
        static::DEFAULT_EMAIL_FORMAT,
        $data['custom_fields']
      );
    }
    catch (\Exception $e) {
      watchdog_exception('acrelianews', $e);
    }
  }

}
